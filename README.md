<h1 style="text-align: center;">
   Marathon Homework Repository
</h1>

----------------------------------------------------------------

### Project Description:

This repository contains the homework assignments for the marathon that took place from December 18 to December 29.</br>
There are a total of 6 assignments, corresponding to the 6 sessions during the marathon.

----------------------------------------------------------------

### Contributors:

The only contributor and in the same time the only person who worked on this project is:

#### `@RYZHAIEV-SERHII`  - _the owner of the repository._

### Project Specifics:

Each homework assignment organized in separate branches, named "homework_1", "homework_3", and so on.</br>
Each branch contain a folder ("day_1", "day_3", ...) with the corresponding assignment text in a markdown
file (`*.md`), and the completed homework.

## Contact:

For any questions or clarifications, feel free to reach out to the owner of repository in ways listed below.

- [Gmail](mailto:rsp89.we@gmail.com)
- [Telegram](https://t.me/CTAJIKEP)
